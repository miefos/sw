<?php

require_once('init.php'); // sets $GLOBALS and autoload classes
include 'includes/header.php';

$prod = new Product();

deleteProducts(); // Validate smth is selected and submit query

?>

<!--  -->
<!-- Product list  -->
<!--  -->
<div class="container">
    <form action="#" method="POST">
        <div class="d-flex  justify-content-end">
            <div class="p-2">
                <select class="form-control">
                    <option value="1" selected>Delete selected</option>
                    <option value="2" Disabled>Modify selected</option>
                </select>
            </div>
            <div class="p-2">
                <button type="submit" class="btn btn-light">Apply</button>
            </div>
        </div>
    
</div>
<div class="container container-products">
        <div class="d-flex flex-wrap">
            <?php echo $prod->getRenderedProducts(); ?>
        </div>
        </form>
</div>


<?php
include 'includes/footer.php';
?>