<?php
require_once('init.php'); // sets $GLOBALS and autoload classes
include 'includes/header.php';

addProduct(); // Validate form and submit query
?>

<!-- Form to add prroduct -->
<div class="row">
    <div class="col-sm-4 add_product">
        <h1>
            Add new product
        </h1>
        <form action="index.php" method="POST">
            <div class="form-group">
                <label for="sku"> SKU </label>
                <input type="text" required class="form-control" name="sku" id="sku">
            </div>
            <div class="form-group">
                <label for="name"> Name </label>
                <input type="text" required  class="form-control" name="name" id="name">
            </div>
            <div class="form-group">
                <label for="price"> Price </label>
                <input type="number" required  class="form-control" name="price" id="price">
            </div>
            <div class="form-group">
                <label for="product-type"> Type Switcher </label>
                <select name="type-switcher" required  id="type-switcher" class="form-control">
                    <option value="" hidden>Choose type</option>
                    <?php  echo Type()->getTypesAsOptions(); ?>
                </select>
            </div>
            <div id="extra">

            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
<!-- End of add product form -->