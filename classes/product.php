<?php

class Product {
    private $_db = null,
            $extra_fields = null;

    public function __construct() 
    {
        $this->_db = DB::getInstance();
    }

    public function insertNewProduct($fields = array())
    {
        $new_extra_fields = array();
        $this->_db->insert('products', $fields['products']);
        foreach ($fields['extra_fields'] as $key => $val) {
            $new_extra_fields['product_id'] = DB::getInstance()->query("SELECT MAX(id) as mi FROM products")->results()[0]['mi'];
            $new_extra_fields['extra_field_id'] = $key;
            $new_extra_fields['field_content'] = $val;
            $this->_db->insert('extra_field_inputs', $new_extra_fields);
        }
        
    }

    public function getProducts()
    {
        return $this->_db->get('products', array())->results();
    }

    public function getAdditionalFields($id, $searched_already) 
    {
        if ($this->extra_fields === null) {
            $this->extra_fields = $this->_db->query("SELECT ef.measurement, ef.field_name, efi.product_id, efi.field_content 
                                    FROM extra_field_inputs AS efi JOIN extra_fields AS ef
                                    ON ef.id = efi.extra_field_id
                                    ORDER BY efi.product_id ",array())->results();
            $min_id = $this->extra_fields[0]['product_id'];
            $searched_already = 0;
        }
       
        $return_val = '';
        for ($i = $searched_already; isset($this->extra_fields[$i]['product_id']); $i++){
            $ef = $this->extra_fields[$i];
            if ($ef['product_id'] == $id) {
                $return_val .= "<div class='" . $ef['field_name'] . "'>";
                $return_val .= $ef['field_name'] . ": "; 
                $return_val .= $ef['field_content'];
                $return_val .= $ef['measurement'];
                $searched_already++;
                $return_val .= "</div>";
            }
        }

        return array($searched_already, $return_val);
    }

    public function getRenderedProducts() 
    {
        $products = $this->getProducts();
        $return_val = '';
        foreach ($products as $product) {
            $return_val .= "<div class='product'>
            <input class='cb' type='checkbox' name='prodCbAction[]' id='cb" . $product['id'] . "' value='" . $product['id'] . "'>
            <div class='product-text'>
            <label for='cb" . $product['id'] . "'>
            <div class='sku'>" . $product['sku'] . 
            " </div>
            <div class='product_name'>" . $product['product_name'] . 
            "</div>
            <div class='product_price'>  Price: " . $product['price'] . 
            " EUR";
            if (isset($additional)){
                $additional = $this->getAdditionalFields($product['id'], $additional[0]);
            } else {
                $additional = $this->getAdditionalFields($product['id'], 0);
            }
            $return_val .= $additional[1];
            $return_val .= "</label></div></div></div>";
        }

        return $return_val;
    }


}


?>