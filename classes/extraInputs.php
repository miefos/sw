<?php

class ExtraInputs {
    private static $_instance = null;
    private $_db = null,
            $inputs = null;

    public function __construct() 
    {
        $this->_db = DB::getInstance();
    }

    public static function getInstance() {
		if (!isset(self::$_instance)){
			self::$_instance = new ExtraInputs();
		}
		return self::$_instance;
	}

    private function setInputs($id)
    {
        $this->inputs = $this->_db->query("SELECT id, CONCAT(field_name, ', ' ,additional) as field_name_plus, field_name, product_type_id, descr FROM extra_fields")->results();
        
    }


    public function getSpecificInputs($id) 
    {
        $inputGroup = array();
        $x = 0;

        foreach ($this->inputs as $input) {
            if ($input['product_type_id'] == $id) {
                $inputGroup[$x]['name'] = $input['field_name'];
                $inputGroup[$x]['id'] = $input['id'];
                $inputGroup[$x]['name_plus'] = $input['field_name_plus'];
                $inputGroup[$x]['descr'] = $input['descr'];
                $x++;
            }
        } 

        return $inputGroup;
    }

    public function getInputs($id) 
    {
        if ($this->inputs === null) {
            $this->setInputs($id);
        }

        $inputGroup = $this->getSpecificInputs($id);



        return $inputGroup;
        
    }


}

?>