// -----------------------------------------------------------
// Get extra fields when type is selected // changed
// -----------------------------------------------------------
$(document).on('change', '#type-switcher', function() {
    $.ajax({type: "POST", 
        url: "functions/functions.php", 
        dataType:"text", 
        data: {selected: $("#type-switcher").find(":selected").val(),
                function_name: 'getExtraInputs'
         }, 
        success:function(response){ 
                $("#extra").html(response);
        }, 
         error: function(xhr, textStatus, errorThrown) { 
                alert('Error!  Status = ' + xhr.status); 
                valueSelected = '';
        }});
});