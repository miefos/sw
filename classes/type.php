<?php

class Type {
    private $_db = null,
            $types = null;

    public function __construct()
    {
        $this->_db = DB::getInstance();
    }

    public function getTypesAsOptions() 
    {
        $this->types = $this->_db->get('product_types')->results();
        $return_val = '';
        foreach ($this->types as $type) {
            $return_val .= "<option value='" . $type['id'] . "'>" . $type['type_name'] . "</option>";
        }
        return $return_val;
    }
}

?>