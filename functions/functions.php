<?php

// Require init.php for ability to use classes
require_once($_SERVER["DOCUMENT_ROOT"] . '/sw/init.php');

// If statement for AJAX called functions 
if (isset($_POST['function_name']) && !empty($_POST['function_name'])) {
  $_POST['function_name']();
  
}

function sanitize($data) 
{
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

// Get extra inputs in index.php (extra fields)
function getExtraInputs()
{
  $inputGroup = ExtraInputs::getInstance()->getInputs($_POST['selected']);

  foreach ($inputGroup as $input) {
      echo "<div class='form-group'>
      <label for='{$input['name']}'> {$input['name_plus']} </label>
      <input type='text' required  class='form-control' name='{$input['name']}'>
      <div class='input_descr'>{$input['descr']}</div>
  </div>";
  }
}


// Make new object functions

function Type () 
{
	return new Type();
}