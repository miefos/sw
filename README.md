Task for scandiweb. 
Product page.

To start:
1) Upload all files to server
2) Update init.php with database info
3) Import database file (db.sql)
4) Start using

Notes: 
1) PHP with plain classes, JQuery used, Bootstrap used, SASS used, PSR-2 mostly used
2) It is possible to add unlimited number of product types and extra fields for them without changing code (in Database). It will be added to types automatically and extra fields will be rendered automatically. View is made in database so products can be seen very easily and comfortable. Might be useful to add indexes if product list becomes large.
3) Mass delete option used 
4) Validation is enabled for main fields and can be added extra validation easily within PHP class but for extra fields only HTML required attribute enabled
5) Data sanitization used
