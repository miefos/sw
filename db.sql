-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 07, 2019 at 02:37 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scandiweb`
--

-- --------------------------------------------------------

--
-- Table structure for table `extra_fields`
--

CREATE TABLE `extra_fields` (
  `id` int(6) NOT NULL,
  `field_name` varchar(50) NOT NULL,
  `additional` varchar(50) DEFAULT NULL,
  `measurement` varchar(20) DEFAULT NULL,
  `descr` varchar(300) DEFAULT NULL,
  `product_type_id` int(11) NOT NULL,
  `insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `extra_fields`
--

INSERT INTO `extra_fields` (`id`, `field_name`, `additional`, `measurement`, `descr`, `product_type_id`, `insert_date`) VALUES
(1, 'size', 'in MB', 'MB', 'Provide size in MB', 1, '2019-03-07 10:51:49'),
(2, 'weight', 'in Kg', 'Kg', 'Provide weight in kg', 2, '2019-03-07 10:51:54'),
(3, 'width', 'in Meters', 'm', 'Provide width in meters', 3, '2019-03-07 10:51:56'),
(4, 'height', 'in Meters', 'm', 'Provide height in meters', 3, '2019-03-07 10:51:59'),
(5, 'length', 'in Meters', 'm', 'Provide length in meters', 3, '2019-03-07 10:52:02');

-- --------------------------------------------------------

--
-- Table structure for table `extra_field_inputs`
--

CREATE TABLE `extra_field_inputs` (
  `id` int(6) NOT NULL,
  `product_id` int(6) NOT NULL,
  `extra_field_id` int(6) NOT NULL,
  `field_content` varchar(100) NOT NULL,
  `insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `extra_field_inputs`
--

INSERT INTO `extra_field_inputs` (`id`, `product_id`, `extra_field_id`, `field_content`, `insert_date`) VALUES
(1, 1, 1, '2048', '2019-03-07 13:31:47'),
(2, 2, 3, '2', '2019-03-07 13:32:13'),
(3, 2, 4, '0,5', '2019-03-07 13:32:13'),
(4, 2, 5, '2', '2019-03-07 13:32:13'),
(5, 3, 2, '2', '2019-03-07 13:33:08'),
(6, 4, 2, '20', '2019-03-07 13:33:28'),
(7, 5, 1, '1024', '2019-03-07 13:35:14');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(6) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_description` varchar(300) DEFAULT NULL,
  `price` int(11) NOT NULL,
  `product_type` int(6) NOT NULL,
  `insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `sku`, `product_name`, `product_description`, `price`, `product_type`, `insert_date`) VALUES
(1, '0001', 'Acme 2GB', NULL, 3, 1, '2019-03-07 13:31:47'),
(2, '0002', 'Bed', NULL, 100, 3, '2019-03-07 13:32:13'),
(3, '0003', 'Harry Potter', NULL, 20, 2, '2019-03-07 13:33:08'),
(4, '0004', 'PHP Programming book', NULL, 500, 2, '2019-03-07 13:33:28'),
(5, '0005', 'Verbatim', NULL, 5, 1, '2019-03-07 13:35:14');

-- --------------------------------------------------------

--
-- Stand-in structure for view `products_full`
-- (See below for the actual view)
--
CREATE TABLE `products_full` (
`id` int(6)
,`sku` varchar(50)
,`product_name` varchar(100)
,`price` int(11)
,`product_description` varchar(300)
,`type_name` varchar(100)
,`extra_field` text
);

-- --------------------------------------------------------

--
-- Table structure for table `product_types`
--

CREATE TABLE `product_types` (
  `id` int(6) NOT NULL,
  `type_name` varchar(100) NOT NULL,
  `type_description` varchar(300) NOT NULL,
  `insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_types`
--

INSERT INTO `product_types` (`id`, `type_name`, `type_description`, `insert_date`) VALUES
(1, 'DVD-disc', 'DVD discs type. Additional field is size in MB.', '2019-03-03 10:58:02'),
(2, 'Book', 'Books are a written or printed work consisting of pages glued or sewn together along one side and bound in covers.', '2019-03-03 10:58:02'),
(3, 'Furniture', 'The movable articles that are used to make a room or building suitable for living or working in, such as tables, chairs, or desks.', '2019-03-03 10:58:34');

-- --------------------------------------------------------

--
-- Structure for view `products_full`
--
DROP TABLE IF EXISTS `products_full`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `products_full`  AS  select distinct `p`.`id` AS `id`,`p`.`sku` AS `sku`,`p`.`product_name` AS `product_name`,`p`.`price` AS `price`,`p`.`product_description` AS `product_description`,`pt`.`type_name` AS `type_name`,group_concat(concat(`ef`.`field_name`,'(',`ef`.`additional`,') :',`efi`.`field_content`) separator '\n') AS `extra_field` from (((`products` `p` join `extra_field_inputs` `efi`) join `extra_fields` `ef`) join `product_types` `pt` on(((`efi`.`product_id` = `p`.`id`) and (`pt`.`id` = `p`.`product_type`) and (`efi`.`product_id` = `p`.`id`) and (`ef`.`product_type_id` = `pt`.`id`) and (`efi`.`extra_field_id` = `ef`.`id`)))) group by `p`.`id` order by `p`.`id` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `extra_fields`
--
ALTER TABLE `extra_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `extra_field_inputs`
--
ALTER TABLE `extra_field_inputs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sku` (`sku`);

--
-- Indexes for table `product_types`
--
ALTER TABLE `product_types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `extra_fields`
--
ALTER TABLE `extra_fields`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `extra_field_inputs`
--
ALTER TABLE `extra_field_inputs`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `product_types`
--
ALTER TABLE `product_types`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
