<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">


<!-- Include JQUERY and JAVASCRIPT -->
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous">
</script>


<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Products</title>
<meta name="description" content="Products">
<meta name="author" content="products.com">
<!-- Stylesheets  and JS-->
<link rel="stylesheet" href="css/css.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="js/js.js"></script>


</head>
<body>

<div class="container-fluid">

<div class="row">
  <div id="navbar" class="col">
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="index.php">Add new product</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="product_list.php">Products List</a>
        </li>
      </ul>
    </nav>
  </div>
</div>