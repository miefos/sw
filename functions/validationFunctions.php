<?php 

// -----------------------------------------------------------
// Form validation for add_product Form
// -----------------------------------------------------------
function addProduct() 
{
  if(Input::exists()) {
    $validate = new Validation();
    $validation = $validate->check($_POST, array(
          'sku' => array(
              'required' => true,
              'exactly' => 4,
              'unique' => 'products'
          ),
          'name' => array(
              'required' => true,
              'min' => 2,
              'max' => 50,
          ),
          'price' => array( 
              'required' => true,
          ),
          'type-switcher' => array(
              'required' => true
          )
    ));
    
    if($validation->passed()) {
          $product = new Product();
          try {
              $extra_fields = array();
              foreach (ExtraInputs::getInstance()->getInputs(Input::get('type-switcher')) as $element) {
                  $idd = $element['id'];
                  $extra_fields[$idd] = Input::get($element['name']);
              }
              $insert_array = array(
                  'products' => array(
                      'sku' => Input::get('sku'),
                      'product_name' => Input::get('name'),
                      'price' => Input::get('price'),
                      'product_type' => Input::get('type-switcher'),
                  ), 'extra_fields' => $extra_fields,
              );
              $product->insertNewProduct($insert_array);
              echo "Successfully inserted!";
          } catch (Exception $e) {
              echo $e->getMessage();
          }
    } else {
      foreach ($validation->errors() as $error) {
        echo $error . '<br />';
      }
    }
  }
  
    
}

// -----------------------------------------------------------
// Delete products from product page
// -----------------------------------------------------------
function DeleteProducts() 
{
  if(Input::exists()) {
    if (isset($_POST['prodCbAction'])) {
        if(!empty($_POST['prodCbAction'])){
            try {
                $sql = "DELETE FROM products WHERE id IN (";
                foreach ($_POST['prodCbAction'] as $p) {
                    $sql .= "?,";
                }
                $sql = substr($sql, 0, -1);
                $sql .= ")";

                DB::getInstance()->query($sql, $_POST['prodCbAction']);

                $sql = "DELETE FROM extra_field_inputs WHERE product_id IN (";
                foreach ($_POST['prodCbAction'] as $p) {
                    $sql .= "?,";
                }
                $sql = substr($sql, 0, -1);
                $sql .= ")";

                DB::getInstance()->query($sql, $_POST['prodCbAction']);
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
    }
  }
}